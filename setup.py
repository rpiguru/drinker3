from setuptools import find_packages, setup


def readme():
    with open('README.md') as f:
        return f.read()

setup(name='visit_classifier',
      version='0.1',
      description='Visit Classifier',
      long_description='Visit Classifier',
      author='John Weis, Caleb Jooren',
      author_email='john.t.weis@gmail.com, cjooren@engineer.com',
      entry_points={
              'console_scripts': [
                  'visit_classifier = visit_classifier.__main__:main',
              ],
          },
      classifiers=[
        'Programming Language :: Python :: 3.6'
      ],
      packages=find_packages(),
      install_requires=[
          'pandas', 'numpy', 'pymongo', 'psycopg2'
      ],
      test_suite='tests',
      package_data={'': ['*.yaml']},
      setup_requires=['pytest-runner'],
      tests_require=['pytest', 'pytest-cov', 'mock'],
      zip_safe=False)
