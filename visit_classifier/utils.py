import datetime
import os
import time
from pymongo import MongoClient
import pandas as pd
import psycopg2
import json
import logging.config
import pymysql

appdir = os.path.abspath(os.path.dirname(__file__))
config = json.loads(open(os.path.join(os.path.dirname(appdir), 'config.json')).read())

mongo_ip = config["MongoDB"]["host"]
client = MongoClient('mongodb://{}:{}/'.format(config['MongoDB']['host'], config['MongoDB']['port']))
db = client[config["MongoDB"]["database"]]
collection = db[config['MongoDB']['collection']]

logging.config.fileConfig(os.path.join(appdir, "logging.ini"))
logger = logging.getLogger("Visit")


def get_disappeared_devices_list():
    """
    Get disappeared device information for each venue
    If a device does not appear for the given time(`DisappearanceTime` in the config), return this device
    :return:
    """
    end_time = int(time.time() - config["EstimationParameters"]['DisappearanceTime'])
    # end_time = int(time.time() - 3600 * 24 * 2)

    grouped_devices = collection.group(key=['venue', "device"],
                                       condition={'latest_ts': {'$lte': end_time}},
                                       initial={'latest_ts': 0},
                                       reduce='function(curr, prev) '
                                              '{prev.latest_ts = Math.max(curr.latest_ts, prev.latest_ts)}')

    s_time = time.time()
    dev_list = []
    for row in grouped_devices:
        dev_info = get_data_of_device(device=row['device'], venue=row['venue'])
        dev_list.append(dev_info)
    if len(grouped_devices) > 0:
        print('Fetched {} disappeared device(s), elapsed: {} s'.format(
            len(grouped_devices), round(time.time() - s_time, 2)))
    return dev_list


def get_data_of_device(device, venue):
    """
    Get ALL documents from the remote mongoDB, and merge into one
    :param device:
    :param venue:
    :return:
    """
    rows = pd.DataFrame(list(collection.find({"device": device, "venue": venue}, {"_id": 0})))

    latest_ts = rows['latest_ts'].max()
    started_ts = rows['started_ts'].min()

    def _get_duration(d, start, last):
        return d if d > 0 else last - start
    rows['dwelling'] = rows.apply(lambda r: _get_duration(r['duration'], r['started_ts'], r['latest_ts']), axis=1)

    duration = rows['dwelling'].sum()
    rssi = rows['rssi'].sum()
    result = {
        'device': device,
        'venue': venue,
        'duration': duration,
        'latest_ts': latest_ts,
        'started_ts': started_ts,
        'rssi': rssi
    }
    return result


def remove_device(device, venue):
    """
    Remove document from the remote mongoDB
    :param device:
    :param venue:
    :return:
    """
    result = collection.delete_many({"device": device, "venue": venue})
    return result.deleted_count


def upload_to_cockroach_db(data):
    host = config['CockroachDB']['host']
    port = config['CockroachDB']['port']
    database = config['CockroachDB']['database']
    user = config['CockroachDB']['user']
    table = config['CockroachDB']['table']

    # FIXME: remove this when using in real field.
    conn = pymysql.connect(host=config['MySQLDB']['host'],
                           user=config['MySQLDB']['user'],
                           password=config['MySQLDB']['password'],
                           db=config['MySQLDB']['database'])

    # conn = psycopg2.connect(host=host, port=port, user=user, database=database)

    cur = conn.cursor()

    # query = 'INSERT INTO {}.{} ("venue_id", "device_id", "start", "duration_inside", "duration_outside")' \
    #         ' VALUES ({}, \'{}\', {}, {}, {})'.format(
    #             database, table, data['venue'], data['device'], data['started_ts'], data['duration'],
    #             data['latest_ts'] - data['started_ts'] - data['duration'])

    start_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(data['started_ts']))
    query = 'INSERT INTO {} (venue_id, device_id, start, duration_inside, duration_outside, visitor_id)' \
            ' VALUES ({}, "{}", "{}", {}, {}, "{}")'.format(
                config['MySQLDB']['table'], 1, 'AC86745F3638', start_time, data.get('duration', 0),
                data['latest_ts'] - data['started_ts'] - data['duration'], data['device'])
    try:
        cur.execute(query)
        conn.commit()
    except Exception as e:
        logger.critical('Failed to execute SQL query(`{}`) - {}'.format(query, e))
    finally:
        cur.close()
        conn.close()
    return True


if __name__ == '__main__':

    get_disappeared_devices_list()
