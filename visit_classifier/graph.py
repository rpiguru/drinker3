import datetime
import json
import os
import psycopg2
import matplotlib.pyplot as plt
import pytz


appdir = os.path.abspath(os.path.dirname(__file__))
config = json.loads(open(os.path.join(os.path.dirname(appdir), 'config.json')).read())


def get_duration_data():
    """
    Get data from the remote CockRoachDB
    :return:
    """
    host = config['CockroachDB']['host']
    port = config['CockroachDB']['port']
    database = config['CockroachDB']['database']
    user = config['CockroachDB']['user']
    table = config['CockroachDB']['table']

    conn = psycopg2.connect(host=host, port=port, user=user, database=database)
    cur = conn.cursor()
    query = 'SELECT "start", "duration_inside" from {}.{}'.format(database, table)
    cur.execute(query)
    rows = cur.fetchall()
    conn.close()
    return rows


def draw_graph():
    rows = sorted(get_duration_data(), key=lambda k: k[0])
    time_list = [datetime.datetime.fromtimestamp(r[0], tz=pytz.utc) for r in rows]
    val_list = [r[1] for r in rows]
    plt.plot(time_list, val_list)
    plt.gcf().autofmt_xdate()
    plt.show()


if __name__ == '__main__':
    draw_graph()
