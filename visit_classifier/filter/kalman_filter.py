import numpy as np
import matplotlib.pyplot as plt
from pymongo import MongoClient

client = MongoClient('mongodb://46.101.172.216:27017/')
db = client['RO']
collection = db['visits']

plt.rcParams['figure.figsize'] = (10, 8)


def kalman_filter(z=None):

    if z is None:
        n_iter = 50
        sz = (n_iter,)  # size of array
        z = np.random.normal(1, 0.1, size=sz)  # observations (normal about x, sigma=0.1)
    else:
        n_iter = len(z)
        sz = (n_iter,)  # size of array

    Q = 1e-5  # process variance

    # allocate space for arrays
    xhat = np.zeros(sz)  # a posteri estimate of x
    P = np.zeros(sz)  # a posteri error estimate
    xhatminus = np.zeros(sz)  # a priori estimate of x
    Pminus = np.zeros(sz)  # a priori error estimate
    K = np.zeros(sz)  # gain or blending factor

    R = 0.1 ** 2  # estimate of measurement variance, change to see effect

    # intial guesses
    xhat[0] = 0.0
    P[0] = 1.0

    for k in range(1, n_iter):
        # time update
        xhatminus[k] = xhat[k - 1]
        Pminus[k] = P[k - 1] + Q

        # measurement update
        K[k] = Pminus[k] / (Pminus[k] + R)
        xhat[k] = xhatminus[k] + K[k] * (z[k] - xhatminus[k])
        P[k] = (1 - K[k]) * Pminus[k]

    plt.figure()
    plt.plot(z, 'k+', label='noisy measurements')
    plt.plot(xhat, 'b-', label='a posteri estimate')
    plt.legend()
    plt.title('Estimate vs. iteration step', fontweight='bold')
    plt.xlabel('Iteration')
    plt.ylabel('RSSI')

    plt.show()


if __name__ == '__main__':
    for m in [c for c in collection.find(filter={'started_ts': {'$gte': 1502773200}})]:
        if m['latest_ts'] - m['started_ts'] > 60 and len(m['rssi']) > 20:
            rssi_list = [r['rssi'] for r in m['rssi']]
            kalman_filter(np.array(rssi_list))
            break
