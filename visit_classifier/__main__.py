import datetime
import os
import pandas as pd
import sys
import time
import numpy

appdir = os.path.abspath(os.path.dirname(__file__))
if appdir not in sys.path:
    sys.path.append(appdir)

# Add parent directory to the system path.
sys.path.append(os.path.abspath(os.path.join(appdir, os.pardir)))

from visit_classifier.utils import config
from visit_classifier.classifier import VisitClassifier
import logging.config
# Initialize Config object and mongo client and collection
from visit_classifier.utils import get_disappeared_devices_list, remove_device, upload_to_cockroach_db


cur_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

logging.config.fileConfig(cur_dir + "logging.ini")
logger = logging.getLogger("Visit")


def main():
    """
    Main function executing record retrieval, analysis and record updates.
    :return:
    """

    estimation_params = config["EstimationParameters"]

    interval = config['interval']

    while True:
        s_time = time.time()
        new_measurements = get_disappeared_devices_list()

        # Loop over records pulled from mongodb
        for m in new_measurements:

            # Skip this set of measurements and do not record a visit if:
            # 1) The time span of measurements is less than the minimum configured for a visit
            # 2) the number of measurements is less than the minimum configured for a visit
            if m['duration'] < estimation_params["MinVisitTime"] \
                    or len(m['rssi']) < estimation_params["MinVisitMeasurementCount"]:
                deleted = remove_device(device=m['device'], venue=m['venue'])
                logger.warning('{} at {} has too short appearance time({}) or samples({}), '
                               'removing: {}'.format(m['device'], m['venue'], m['duration'], len(m['rssi']), deleted))
                continue
            elif m['duration'] >= estimation_params['MaxVisitTime']:
                deleted = remove_device(device=m['device'], venue=m['venue'])
                logger.warning('{} at {} has too long time({}), which seems to be wrong, '
                               'removing: {}'.format(m['device'], m['venue'], m['duration'], deleted))
                continue

            # Convert RSSI measurment list to pandas.DataFrame
            rssi_df = pd.DataFrame(m['rssi'])

            # Create new VisitClassifier instance with parameters that define the minimum number of measurements and the
            # minimum time span of a visit
            vc = VisitClassifier(min_count=estimation_params["MinVisitMeasurementCount"],
                                 min_visit_time_sec=estimation_params["MinVisitTime"])

            # Identify visits that occur in the set of RSSI measurements. Here multiple visits may be identified if a
            # device exits and reenters a venue, however only one visit will be recorded for each device.
            current_visit_df = vc.find_visits(rssi_df, estimation_params["MaxEntryCertainRange"],
                                              max_visit_dt=estimation_params["VisitBreakMaxDT"])

            # Record a single visit if any visits are found, regardless of how many and update record in mongodb
            if any(current_visit_df.visit >= 1):
                # Duration should be the sum of row's timedelta which has certain distance.
                m['duration'] = current_visit_df[
                    current_visit_df['filtered_range'] <= estimation_params["MaxEntryCertainRange"]]['dt'].sum()

                if numpy.isnan(m['duration']):
                    m['duration'] = 0

                upload_to_cockroach_db(m)
                start_time = datetime.datetime.fromtimestamp(m['started_ts']).isoformat()
                end_time = datetime.datetime.fromtimestamp(m['latest_ts']).isoformat()
                logger.debug('Uploaded: device: {}, venue: {}, start: {}, end: {}, duration: {}'.format(
                    m['device'], m['venue'], start_time, end_time, m['duration']))
            else:
                logger.warning('{} at {} does not meet requirements'.format(m['device'], m['venue']))

            remove_device(device=m['device'], venue=m['venue'])

        elapsed = time.time() - s_time
        if elapsed < interval:
            time.sleep(interval - elapsed)


if __name__ == '__main__':
    logger.debug('========== Starting Visit Classifier ==========')
    main()
