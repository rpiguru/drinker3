import pandas as pd
import numpy as np
from visit_classifier.range_state_estimate import RangeStateEstimate
pd.options.mode.chained_assignment = None


class VisitClassifier:
    """
    Identifies visits from RSSI measurements
    """
    def __init__(self, min_count=5, min_visit_time_sec=60):
        """
        Constructor
        :param min_count: (int) Minimum number of measurements required to attempt visit identification.
        :param min_visit_time_sec: (int) Minimum amount of time a set of must span  to attempt visit identification.
        """
        self.min_count = min_count
        self.min_visit_time_sec = min_visit_time_sec

        # Distance calculation from previous node.js version
        self.original_distance_calc = lambda rssi: 10 ** ((-51 - rssi) / (10 * 2))

        # Parameterixed distance calculation with the followin parameters:
        # P: (int) Transmitter power in dbm
        # rssi: (int) Received Signal Strength Indicator
        # n: (float) Signal propagation constant.
        #            Ex: n=2 for free propagation in open air
        #                n=4 for indoor wifi propagation
        self.parameterized_distance_calc = lambda P, rssi, n: 10 ** ((P - rssi) / (10 * n))

    def append_kinematics(self, measurement_df):
        """
        Appends kinematic estimate fields to measurement_df
        :param measurement_df: (pandas.DataFrame) DataFrame containing the fields ts (timestamp)
                                and rssi (Received Signal Strength Indicator measurement)
        :return measurement_df: (pandas.DataFrame) Input data frame with appended fields dt, range, range_dot
                                                    and range_dot_dot
        """
        measurement_df['dt'] = measurement_df['ts'].diff()

        # Calculate range estimates from RSSI measurements
        measurement_df['range'] = measurement_df['rssi'].apply(self.original_distance_calc)

        # Calculate range velocity (range_dot) estimates from range estimates
        measurement_df['range_dot'] = measurement_df[['ts', 'range']].diff().apply(lambda row: row['range'] / row['ts'],
                                                                                   axis=1)

        # Calculate range acceleration (range_dot_dot) estimates from range_dot estimates
        measurement_df['range_dot_dot'] = measurement_df[['ts', 'range_dot']].diff().apply(
            lambda row: row['range_dot'] / row['ts'], axis=1)

        return measurement_df

    @staticmethod
    def append_filtered_state(measurement_df):
        """
        Appends filtered kinematic estimate fields to measurement_df using Kalman filtering
        :param measurement_df: (pandas.DataFrame) DataFrame containing the fields ts, dt, range, range_dot
                                                    and range_dot_dot
        :return measurement_df: (pandas.DataFrame) Input data frame with appended fields filtered_range,
                                                    filtered_range_dot, filtered_range_dot_dot and cov
        """
        filtered_states = []

        # Set initial squared measurement error
        R_0 = 5

        # Set initial state vector to the first full state estimate (i.e. the first measurement with estimated
        # range_dot and renage_dot_dot fields)
        x_init = measurement_df.iloc[2][['range', 'range_dot', 'range_dot_dot']].values.reshape((3, 1))

        # Initialize RangeStateEstimate instance
        r = RangeStateEstimate(x_init, measurement_df.iloc[2]['ts'], np.identity(3), np.zeros(3))
        for t, meas in measurement_df[['ts', 'range']].values:
            # Set initial squared measurement error
            R = R_0

            # Make measurement squared error smaller for smaller range measurements
            # Error in range is due to multi-path scattering effects and occlusion
            # thus larger range measurements are more likely to be erroneous
            if r.x[0] > meas:
                R = 0.5

            # Update and retain state estimate using next measurement
            new_state = r.update(t, meas, R)  # (ts, state, cov)
            r.retain_state(*new_state)

            # Append new state estimate as dict to filtered_states list
            new_state_dict = {'ts': new_state[0],
                              'filtered_range': float(new_state[1][0]),
                              'filtered_range_dot': float(new_state[1][1]),
                              'filtered_range_dot_dot': float(new_state[1][2]),
                              'cov': new_state[2]}
            filtered_states.append(new_state_dict)

        # Create DataFrame from filtered_states list and join with input measurement_df
        filtered_state_df = pd.DataFrame(filtered_states)
        measurement_df = measurement_df.merge(filtered_state_df, how='left', on='ts')

        return measurement_df

    def find_visits(self, measurement_list, max_venue_visit_range, max_visit_dt=300):
        """
        Identifies visits within a given set of measurements
        :param measurement_list: (list) List of dicts corresponding to RSSI measurements from mongodb
        :param max_venue_visit_range: (float) Maximum range to initiate a visit (i.e. a set of measurements must have at
                                                least one measurement less than or equal to max_venue_visit_range to
                                                perform analysis).
        :param max_visit_dt: (int) Maximum time between measurements allowed for a single visit to continue.
                                    This parameter is meaningful for identifying multiple visits by a single device.
        :return measurement_df: (pandas.DataFrame) DataFrame containing the following fields:
                                    ts: (int) Timestamp
                                    dt: (int) Time between current and previous measurement
                                    range: (float) Range measurement from RSSI
                                    range_dot: (float) Range velocity estimate
                                    range_dot_dot: (float) Range acceleration estimate
                                    filtered_range: (float) Range estimate from Kalman filter
                                    filtered_range_dot: (float) Range velocity estimate from Kalman filter
                                    filtered_range_dot_dot: (float) Range acceleration estimate from Kalman filter
                                    cov: (numpy.matrix, shape=(3,3) State estimate covariance matrix from Kalman filter
                                    visit: (int) Integer indicating the individual visit a measurement corresponds to.
                                                 NB: A value of -1 indicates the measurement does not correspond
                                                     to a visit.
                                    Note: The kinematic fields above will only be included if the measurement set passes
                                            the set of initial pre-analysis tests.
        """
        measurement_df = pd.DataFrame(measurement_list)
        # Do not perform analysis if any of the following conditions are not met:
        # 1) Sufficient measurements
        # 2) Sufficient time span to contain a visit
        # 3) At least one measurement is within range to consider a visit
        #       (This last condition may need to be modified to incorporate measurement error)
        if (len(measurement_list) < self.min_count
            or (measurement_df['ts'].max() - measurement_df['ts'].min()) < self.min_visit_time_sec
                or self.original_distance_calc(measurement_df['rssi'].max()) > max_venue_visit_range):
            measurement_df['visit'] = None
        else:
            # Add dt, range, range_dot, and range_dot_dot fields
            measurement_df = self.append_kinematics(measurement_df)

            # At this point we can split any potential visits based on the time between measurements and the
            # max_visit_dt parameter.

            # Get the list of indices for measurements whose dt value is greater than max_visit_dt.
            visit_splits = measurement_df[(measurement_df['dt'] > max_visit_dt)].index
            visit_start = 0
            ix = 0
            # Iterate over the found indices and append incremental visit indices to the measurements in the rows
            # specified by the indices.
            for ix, vs in enumerate(visit_splits):
                measurement_df.loc[visit_start:vs - 1, 'visit'] = ix
                visit_start = vs
            measurement_df.loc[visit_start:, 'visit'] = ix + 1

            # Now actual visits can be determined based on the filtered range estimates

            # Add filtered state estimates for each potential visit separately
            new_measurement_df = pd.DataFrame()
            for g in measurement_df.groupby('visit'):
                if g[1].count().max() > 2:
                    tmp_measurement_df = self.append_filtered_state(g[1])
                    new_measurement_df = new_measurement_df.append(tmp_measurement_df)

            measurement_df = new_measurement_df.reset_index(drop=True)

            # Label measurement spans outside of range with visit index of -1 (not a valid visit)
            if 'filtered_range' in measurement_df.columns:
                out_of_range_ix = measurement_df[(measurement_df['filtered_range'] > max_venue_visit_range)].index
                span_out_of_range = []
                # This parameter is the maximum number of sequential Out-of-Range measurements required to 'break' the
                # current visit and increment the visit id's of any future visits
                min_span_length = 5  # Min number of measurements outside of range
                i = 0
                # Iterate over all indices of out of range measurements and check if the next min_span_length
                # measurements are also out of range. If a span of min_span_length measurements are found to be out of
                # range 'break' the current visit and increment the visit id's of any future visits using
                while i < len(out_of_range_ix):
                    incl_next = False
                    current_ix = out_of_range_ix[i]
                    if i < len(out_of_range_ix) - min_span_length:
                        incl_next = all(
                            [out_of_range_ix[i + j] == current_ix + j for j in range(1, min_span_length + 1)])
                    else:
                        break
                    if incl_next:
                        span_out_of_range.extend([current_ix + j for j in range(min_span_length + 1)])
                        i += min_span_length + 1
                    else:
                        i += 1
                measurement_df.loc[span_out_of_range, 'visit'] = -1
            else:
                measurement_df['visit'] = -1
                return measurement_df

            # Reindex visits to account for any breaks found above
            pair_list = []
            for i in measurement_df.index[measurement_df['visit'].diff() < 0]:
                for j in measurement_df.index[measurement_df['visit'].diff() > 0]:
                    if i < j:
                        pair_list.append((i, j))
                        break
            for i, p in enumerate(set(pair_list)):
                ix = p[(i + 1) % 2]
                if i % 2:
                    ix -= 1
                measurement_df.loc[ix:, 'visit'][measurement_df.loc[ix:, 'visit'] > -1] += 1
            measurement_df['ts'] = pd.to_numeric(measurement_df['ts'])
        return measurement_df
